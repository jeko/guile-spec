(use-modules
 (jeko-packages)
 (gnu packages guile)
 (gnu packages code)
 (guix packages)
 (guix build-system guile)
 (guix build utils)
 (guix licenses)
 (guix git-download)
 (guix gexp)
 (ice-9 popen)
 (ice-9 rdelim))

(define source-dir (dirname (current-filename)))

(define git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ)))

(package
 (inherit guile-spec)
 (name "guile-spec-git")
 (version (string-append (package-version guile-spec) "-HEAD"))
 (source (local-file source-dir
		     #:recursive? #t
		     ;;#:select? (not (basename ".git"))
		     ))
 (inputs (list guile-3.0-latest))
 (propagated-inputs (list lcov)))

