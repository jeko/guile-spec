(add-to-load-path "..")

(use-modules (spec)
	     (ice-9 exceptions)
	     (srfi srfi-64))

(install-spec-runner-repl)

(define run-tests
  (lambda ()
    (describe "a test suite"
      (it "should be true"
	(should #t))
      (it "should be equal to 1"
	(should= 1 1)))
    
    (describe "another test suite"
      (it "should not be false"
	(should-not #f))
      (it "should throw exception"
	(should-throw warning? (raise-exception (make-warning))))
      (it "runs even after the previous test has thrown"
	(should= "3.0" (effective-version))))
    
    (describe "a test suite with context"
      (context "fail"
	(it "assertion"
	  (should #f))
	(it "equality"
	  (should= 1 2)))
      (context "pass"
	(it "assertion"
	  (should (number? 123456)))
	(it "equality"
	  (should= "hello world" (string-concatenate (list "hello" " " "world"))))))))
