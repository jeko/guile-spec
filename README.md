# Guile Spec - Sugar for SRFI-64 - Inspired by [Speclj](https://github.com/TakaGoto/speclj)

## Try it first !

Guile Spec is distributed with a `guix.scm` file which can be use to create a virtual environment to host your experiments. You can spawn it as follow :
```bash
$ guix shell 
$ cd examples
$ ./run-tests.sh # simply run the tests
$ ./run-test-coverage.sh # run the tests and test the code coverage rate
```
**Bonus : generate html report**
```bash
$ mkdir -p /tmp/spec-lcov & rm -r /tmp/spec-lcov & mkdir /tmp/spec-lcov
$ genhtml lcov.info --output-directory=/tmp/spec-lcov
```

## Install

Until Guile Spec is available in Guix repository, you use my custom channel. To do so, add these lines to your `~/.config/guix/channels.scm` (adapt it if needed) :
```scheme
(cons (channel
       (name 'jeko)
       (url "git@framagit.org:Jeko/jeko-channel.git"))
      %default-channels)
```
And then run :
```bash
$ guix pull
$ guix install guile-spec # or guile2.2-spec for Guile 2.2.x compatibility
```

## Usage

Guile Spec provides you with macros and procedures to write tests and read results from the command line or in the REPL.

### From the command line

Put some tests in a file (for example: test-it.scm) :
```scheme
;; First, import the module !
(use-modules (spec))
;; Second, install the runner which handles the test results outputs
(install-spec-runner-term)
;; Third, write some tests
(describe "first-test"
  (it "runs as expected"
    (should #t)))
```
Run the tests from the command line and see the result :
```bash
$ guile --debug test-it.scm
;;; note: auto-compilation is enabled, set GUILE_AUTO_COMPILE=0
;;;       or pass the --no-auto-compile argument to disable.
;;; compiling /home/jeko/Workspace/guile-spec/test-it.scm
;;; compiled /home/jeko/.cache/guile/ccache/3.0-LE-8-4.2/home/jeko/Workspace/guile-spec/test-it.scm.go
first-test
     PASS runs as expected
```

### From the REPL

Put some tests in a file (for example: test-it.scm) :
```scheme
;; First, import the module !
(use-modules (spec))
;; Second, install the runner which handles the test results outputs
(install-spec-runner-repl)
;; Third, write some tests
(define run-tests
  (lambda ()
    (describe "first-test"
      (it "runs as expected"
        (should #t)))))
```
Load the file in the REPL then evaluate the `run-tests` procedure :
```scheme
scheme@(guile-user)> (run-tests)
first-test
     PASS runs as expected
```
