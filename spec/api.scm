(define-module (spec api)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 exceptions)
  #:export (describe it context))

(define-syntax describe
  (syntax-rules ()
    ((_ name spec-components spec-components* ...)
     (begin
       (test-begin name)
       spec-components spec-components* ...
       (test-end name)))))

(define-syntax context
  (syntax-rules ()
    ((_ name spec-components spec-components* ...)
     (test-group name
       spec-components spec-components* ...))))

(define-syntax it
  (syntax-rules (should should-not should= should-throw)
    [(_ characteristic (should assertion))
     (test-assert characteristic assertion)]
    [(_ characteristic (should-not assertion))
     (test-assert characteristic (not assertion))]
    [(_ characteristic (should= expected actual))
     (test-equal characteristic expected actual)]
    [(_ characteristic (should-throw error-type-predicate expression))
     (test-assert characteristic
       (with-exception-handler
	   (lambda (e)
	     (error-type-predicate e))
	 (lambda ()
	   expression)
	 #:unwind? #t))]))
