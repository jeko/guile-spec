(define-module (spec coverage)
  #:use-module (system vm coverage)
  #:use-module (system vm vm)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 ports)
  )

(define-public (test-coverage test-suite)
  (lcov-total-line-coverage-rate (lcov-report-total (lcov-data test-suite)))
  )

(define (lcov-data test-suite)
  (call-with-values
      (lambda ()
	(with-code-coverage (lambda () (primitive-load test-suite))))
    (lambda (coverage-data test-results)
      (let ([source-under-test (format #f "*~a/*" (substring test-suite 0 (string-index test-suite #\-)))]
	    [lcov-data-filename "lcov.info"])
	(begin
	  (if (file-exists? lcov-data-filename)
	      (delete-file lcov-data-filename))
	  (call-with-output-file lcov-data-filename
	    (lambda (lcov-report-port)
	      (coverage-data->lcov coverage-data  lcov-report-port)))
	  (system* "lcov" "--extract" lcov-data-filename source-under-test "-o" lcov-data-filename)
	  lcov-data-filename)))))

(define (lcov-report-total lcov-data)
  (call-with-input-pipe (string-append "lcov -l " lcov-data)
			(lambda (lcov-pipe)
			  (let loop ([current-line (read-line lcov-pipe)]
				     [last-line-read ""])
			    (if (eof-object? current-line)
				last-line-read
				(loop (read-line lcov-pipe)
				      current-line))))))

(define (lcov-total-line-coverage-rate lcov-report-total)
  (let* ([total (match:substring
		 (string-match
		  "[0-9]?[0-9]?[0-9]\\.?[0-9]?"
		  lcov-report-total))])
    (string->number total)))

(define (call-with-input-pipe pipe proc)
  (let ([p (open-input-pipe pipe)])
    (call-with-values
	(lambda () (proc p))
      (lambda vals
	(close-pipe p)
	(apply values vals)))))


