(define-module (dummy dummy))

(define-public (return-0)
  0)

(define not-used
  (lambda (critera)
    (if (eqv? critera 'serve-as-illustration-to-exercise-coverage)
	"oops"
	"never mind")))
