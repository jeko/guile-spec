(use-modules (spec))

(install-spec-runner-term)

(describe "code-coverage"
  (it "remains very high"
    (should (>= (test-coverage "dummy-test.scm") 90.0))))
